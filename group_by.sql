-- Active: 1695817678749@@127.0.0.1@3306@employees

-- Sur la table titles, faire un group by pour afficher combien de titles chaque employees a eu
SELECT emp_no, COUNT(*) title_nb FROM titles GROUP BY emp_no;

-- Même requête mais avec une petite jointure pour avoir aussi les noms et prénom de l'employee
SELECT emp_no, first_name, last_name, COUNT(*) title_nb FROM titles NATURAL JOIN employees GROUP BY emp_no; --ne marche que dans le cas où les colonnes de la jointure ont le même nom

-- SELECT t.emp_no, first_name, last_name, COUNT(*) title_nb FROM titles t INNER JOIN employees e ON e.emp_no=t.emp_no GROUP BY t.emp_no;

-- Faire une requête pour avoir le salaire moyen actuel dans l'entreprise (pas de group by à priori)
SELECT AVG(salary) FROM salaries WHERE to_date >= NOW();

-- Faire une requête pour avoir le salaire moyen et max et min apparemment de chaque employee
SELECT CONCAT(first_name,' ', last_name) fullname, MIN(salary) salary_min, AVG(salary) salary_avg, MAX(salary) salary_max FROM salaries NATURAL JOIN employees GROUP BY emp_no ORDER BY salary_max DESC LIMIT 10;

-- Faire une requête avec jointure pour afficher le salaire moyen actuel par department (on peut faire 2 jointure pour avoir le nom du department aussi)
SELECT AVG(salary) avg_salary, dept_name FROM salaries NATURAL JOIN dept_emp NATURAL JOIN departments GROUP BY dept_no ORDER BY avg_salary DESC;

-- Faire une requête pour afficher les employees et leur title et salaire actuel
SELECT e.*, s.salary, t.title FROM employees e INNER JOIN titles t ON t.emp_no=e.emp_no  INNER JOIN salaries s ON s.emp_no=e.emp_no WHERE s.to_date >= NOW() AND t.to_date >= NOW() ORDER BY emp_no;
-- Afficher le nom des Departments et le nom de leur manager actuel·le
SELECT d.dept_name, CONCAT(e.first_name, ' ', e.last_name) FROM departments d INNER JOIN dept_manager dm ON d.dept_no=dm.dept_no INNER JOIN employees e ON e.emp_no=dm.emp_no WHERE dm.to_date >= NOW();

-- Afficher le salarié qui a le salaire le plus élevé de l'entreprise et son title actuel
SELECT CONCAT(first_name,' ', last_name) fullname, MIN(salary) salary_min, AVG(salary) salary_avg, MAX(salary) salary_max FROM salaries NATURAL JOIN employees GROUP BY emp_no ORDER BY salary_max DESC LIMIT 2;
SELECT CONCAT(first_name,' ', last_name) fullname FROM salaries NATURAL JOIN employees ORDER BY salary DESC LIMIT 1;

-- Afficher le pourcentage actuel de femme dans chaque department
SELECT dept_name, COUNT(IF(e.gender='F', 1, null)) * 100/COUNT(*) AS percent_f FROM departments d INNER JOIN dept_emp de ON d.dept_no=de.dept_no INNER JOIN employees e ON de.emp_no=e.emp_no GROUP BY d.dept_no;

-- Afficher les employees dont le salaire moyen est supérieur à 80000
SELECT CONCAT(first_name,' ', last_name) fullname, AVG(salary) salary_avg FROM salaries NATURAL JOIN employees GROUP BY emp_no HAVING salary_avg >= 80000 ORDER BY salary_avg DESC;