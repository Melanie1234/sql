-- Nouvelle procédure : raise_employee qui va attendre en argument le emp_no de l'employee à augmenter et le raise_percentage de combien de pourcent on l'augmente.
-- Récupérer le salaire actuel de l'employee
-- Update le salaire actuel pour mettre sa to_date à maintenant
-- Créer un nouveau salaire avec le nouveau salaire calculé à partir de l'ancien + pourcentage

-- Solution :


DROP PROCEDURE IF EXISTS raise_employee;

CREATE PROCEDURE raise_employee(IN empNo INT, IN raise_percentage INT)
BEGIN
    DECLARE raised_salary INT;

    SELECT (salary+salary*raise_percentage/100) INTO raised_salary FROM salaries WHERE emp_no=empNo AND to_date >= NOW();
    -- ça c'était pas demandé, mais si on veut que le salaire des gens ne dépasse pas 160000
    IF raised_salary > 160000 THEN
        SET raised_salary= 160000;
    END IF;

    UPDATE salaries SET to_date=NOW() WHERE emp_no=empNo AND to_date>=NOW();

    
    INSERT INTO salaries (emp_no,salary, from_date, to_date) VALUES (empNo, raised_salary, NOW(), '9999-01-01');

END;