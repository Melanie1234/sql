DROP PROCEDURE IF EXISTS hire_employee;

CREATE PROCEDURE hire_employee(IN firstName VARCHAR(14), IN lastName VARCHAR(16), IN birthDate DATE, IN genderEmp ENUM('M','F'), IN salaryEmp INT, IN titleEmp VARCHAR(50))
BEGIN
    DECLARE empNo INT;

    SELECT (emp_no+1) INTO empNo FROM employees ORDER BY emp_no DESC LIMIT 1;

    INSERT INTO employees (emp_no,first_name,last_name, birth_date, gender, hire_date) VALUES (empNo,firstName,lastName,birthDate,genderEmp, NOW());

    INSERT INTO titles (emp_no, from_date, to_date, title) VALUES (empNo, NOW(), '9999-01-01', titleEmp);

    INSERT INTO salaries (emp_no, from_date, to_date, salary) VALUES (empNo, NOW(), '9999-01-01', salaryEmp);

END;