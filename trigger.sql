DROP TRIGGER IF EXISTS assign_department;

CREATE TRIGGER assign_department AFTER INSERT ON employees FOR EACH ROW
BEGIN
    IF NOT EXISTS(SELECT * FROM departments WHERE dept_name='OnBoarding') THEN
        INSERT INTO departments (dept_no,dept_name) VALUES ('d010', 'OnBoarding');

    END IF;

    INSERT INTO dept_emp (emp_no,dept_no, from_date, to_date) VALUES (NEW.emp_no, 'd010', NOW(), '9999-01-01');
END;